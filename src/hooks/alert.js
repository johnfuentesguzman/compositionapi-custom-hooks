import { ref } from 'vue';
export default function useAlert(){// the prefix "use..." is a usual way to called your custom hooks
    const alertIsVisible = ref(false);

    function showAlert() {
    alertIsVisible.value = true;
    }
    function hideAlert() {
    alertIsVisible.value = false;
    }

    function testingParameter(p){
        alert(p);
    }

    return [
    alertIsVisible,
    showAlert,
    hideAlert,
    testingParameter
    ];
};